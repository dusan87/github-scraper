from django import forms


class GithubNavigatorForm(forms.Form):
    search_term = forms.CharField()

    def clean_search_term(self):
        search_term = self.cleaned_data['search_term']
        return search_term.strip()

