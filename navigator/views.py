from django.shortcuts import redirect
from django.views.generic import TemplateView, View
from github.utils import search_github_repositories
from navigator.forms import GithubNavigatorForm
from github.exceptions import GithubAPIException


class RedirectToNavigatorView(View):
    """It just redirects first access to /navigator"""
    def get(self, request, *args, **kwargs):
        return redirect('/navigator/?search_term=default')


class GithubNavigatorView(TemplateView):

    http_method_names = ['get']
    template_name = 'template.html'

    def get(self, request, *args, **kwargs):
        form = GithubNavigatorForm(request.GET)

        if not form.is_valid():
            kwargs['form_errors'] = form.errors
        else:
            try:
                search_term = form.cleaned_data['search_term']
                kwargs['search_term'] = search_term
                kwargs['repositories'] = search_github_repositories(search_term)
            except GithubAPIException as e:
                kwargs['api_error'] = e.message

        return super().get(request, *args, **kwargs)
