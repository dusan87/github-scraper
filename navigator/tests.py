from django.shortcuts import reverse
from django.test import TestCase, mock
from github.proxy import GithubProxySession
from github.utils import search_github_repositories
from github.exceptions import GithubAPIException


class MockRequestsResponse(object):
    def __init__(self, data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data


def mocked_github_request_get(*args, **kwargs):
    _, url_arg = args

    commits_url = GithubProxySession.GITHUB_API_BASE_URL + '/mockrepo/test/commits'
    empty_repo_commits_error_url = GithubProxySession.GITHUB_API_BASE_URL + '/mockrepo/error/commits'

    if url_arg == '/search/repositories' and kwargs.get('params') == 'q=test+in:name':
        return MockRequestsResponse({'items': [
            {
                'created_at': '2017-07-23T14:21:26Z',
                'name': 'arrow',
                'owner': {
                    'avatar_url': 'https://avatar.png/',
                    'html_url': 'https://url.html',
                    'login': 'test'
                },
                'commits_url': commits_url
            },
            {
                'created_at': '2017-08-23T14:21:26Z',
                'name': 'arrow',
                'owner': {
                    'avatar_url': 'https://avatar.png/',
                    'html_url': 'https://url.html',
                    'login': 'test'
                },
                'commits_url': empty_repo_commits_error_url
            }
        ]}, 200)

    if url_arg == '/search/repositories' and kwargs.get('params') == 'q=nodata+in:name':
        return MockRequestsResponse({'items': []}, 200)

    if url_arg == commits_url:
        return MockRequestsResponse([{
            'sha': '7ed71fa0341bed2fef5bac11d2fe44355184a1da',
            'commit': {
                'message': 'Initial Commit',
                'author': {
                    'name': 'Test Author'
                }
            }
        }], 200)

    if url_arg == empty_repo_commits_error_url:
        return MockRequestsResponse({'message': 'Github empty repository'}, 409)

    return MockRequestsResponse({'message': 'Bad request'}, 404)


def mocked_github_request_get_rate_limit_error(*args, **kwargs):
    return MockRequestsResponse({'message': 'API rate limit exceeded..'}, 403)


class GithubSearchRepositoriesTestCase(TestCase):

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get_rate_limit_error)
    def test_search_github_repositorues_return_rate_limit_error(self):
        with self.assertRaises(GithubAPIException) as e:
            search_github_repositories(search_term='test')

        self.assertIn('rate limit', e.exception.message)

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get)
    def test_search_github_repositories_return_no_data(self):
        data = search_github_repositories(search_term='nodata')
        self.assertEqual(data, [])

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get)
    def test_search_github_repositories_return_repos_sorted_by_created_date(self):
        """
        It tests that results are sorted by `created_at` value in descending order
        """
        data = search_github_repositories(search_term='test')
        self.assertEqual(len(data), 2)
        self.assertGreater(data[0]['created_at'], data[1]['created_at'])

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get)
    def test_search_github_repositories_return_repos_where_one_has_no_commits(self):
        """
        It tests the message under commit details when one repository has no commits/empty repo.
        """
        data = search_github_repositories(search_term='test')
        self.assertEqual(len(data), 2)
        self.assertIn('commit_error_message', data[0])
        self.assertNotIn('commit_error_message', data[1])


class GithubNavigatorIntegrationTestCase(TestCase):

    def setUp(self):
        self.url = reverse('github-navigator-view')

    def test_search_term_has_no_value_should_raise_form_errors(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertIn('form_errors', response.context)
        self.assertIn('search_term', response.context['form_errors'])

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get_rate_limit_error)
    def test_search_github_repositories_should_raise_github_api_error(self):
        """It tests that response context contains `api_error` which will be rendered"""

        response = self.client.get(self.url, {'search_term': 'test'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('api_error', response.context)

    @mock.patch.object(GithubProxySession, 'get', mocked_github_request_get)
    def test_search_github_repositories_return_list_of_repos_data(self):
        """It tests that response context contains searched repositories items."""
        response = self.client.get(self.url, {'search_term': 'test'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('repositories', response.context)
        self.assertEqual(len(response.context['repositories']), 2)
