# README #

Scraper Github - It fetches github repositories searched by given `search_term`. 
The search is based on using qualifier `+in:name`, which renders only repositories which contain the `search_term` value in their name.

### Main modules/functions ###
 1. `github.utils.py` --> function `search_github_repositories` (fetch repositories)
 2. `navigator.views.py` --> class `GithubNavigatorView` request handler
 3. `navigator.tests.py` --> tests
 4. `cover.txt` or `README.md` --> application insights

### Task Explanation ###
 1. The main logic is based in `github.utils.search_github_repositories` function. Where we receive `search_term` as mandatory argument.
    We parse fetched repositories data that are required for this task and sort the newest 5 repositories by `created_at` field.
    Moreover, for each repository data we make additional request the fetch the last commit using `commits_url` value provided withing repository data.
 2. There are three types of error handling.
    - `search_term` missing field error. If for any reason `search_term` is empty or not provided at all.
      We do display the error that the value must be provided in order to process request.
    - Top level error when we try to search for repository.
      The errors we may face in this level are `RateLimit`, `Unauthorized` (if we used API_TOKEN) and so forth.
      If such a error occur we just render error message in template and nothing else.
    - Inline errors once the repositories are fetched. To obtain the last commit for each of 5 newest repositories, we do call again gituhub Api.
      In this stage a repository does not necessary have commits (e.g empty repo). So, we do handle this error and show under "Last commit" a message "Github empty repository".
 3. under `navigator.views.py` we handle GET request "/navigator/?search_term=whatever"
 3. There are provided tests for this flow. The all tests are localed under `navigator.tests.py` module. (How to run them please see below in last chapter "Running tests")
 4. GITHUB_API_TOKEN is optional. If you want to have higher rate limit you might want to add your github token under `scrapper.settings.py`.

### Project stack ###
* Python 3.6 (it works on all the other python versions 2.7+)
* Django 1.11

### Getting up and running ###

The steps below will get you up and running with a local development environment. I assume you have the following installed:

* pip
* virtualenv

Clone the project

    $ git clone git@bitbucket.org:dusan87/github-scraper.git

First make sure to create and activate a _virtualenv_ (http://docs.python-guide.org/en/latest/dev/virtualenvs/)

    $ pip install virtualenv
    $ cd /path/to/github-scraper/
    $ virtualenv github-scraper-env
    $ source github-scraper/bin/activate

then install the requirements for local development:

    $ pip install -r requirements.txt

You can now run the ``application`` command::

    $ python manage.py runserver 0.0.0.0:8000
    
You should be able to open the application and load repositories data for a given `search_term` value:

    http://127.0.0.1:8000/

### Running tests ###

All tests are located under `tests.py` file. To run them execute the command::

    $ python manage.py test

### Makefile bootstrap ###
    You may want you use Makefile

    $ make bootstrap

_NOTE_: Makefile.virtualenv points on /usr/local/bin/python3. Please change if this is different on your OS.
