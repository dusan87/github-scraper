SHELL := /bin/sh

.PHONY: virtualenv install start test

virtualenv:
	pip install virtualenv
	virtualenv -p /usr/local/bin/python3 github-scraper-env

install:
	( \
       source github-scraper-env/bin/activate; \
       pip install -r requirements.txt; \
	)

start:
	github-scraper-env/bin/python manage.py runserver 0.0.0.0:8000

test: virtualenv install
	github-scraper-env/bin/python manage.py test

bootstrap: virtualenv install start
