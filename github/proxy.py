from django.conf import settings
from requests import Session


class GithubProxySession(object):
    GITHUB_API_BASE_URL = 'https://api.github.com'

    def __init__(self):
        self._init_session()

    def _init_session(self):
        session = Session()
        if settings.GITHUB_API_TOKEN:
            session.headers['Authorization'] = 'token {}'.format(settings.GITHUB_API_TOKEN)
        self.session = session

    def get_full_url(self, url):
        if self.GITHUB_API_BASE_URL in url:
            return url
        return "{}{}".format(self.GITHUB_API_BASE_URL, url)

    def get(self, url, **kwargs):
        url = self.get_full_url(url)
        return self.session.get(url, **kwargs)
