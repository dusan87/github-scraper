from github.proxy import GithubProxySession
from github.exceptions import GithubAPIException

HTTP_200_OK = 200


def _get_github_repository_details(repo_data):
    data = {
        'avatar_url': repo_data['owner']['avatar_url'],
        'owner_url': repo_data['owner']['html_url'],
        'owner_login': repo_data['owner']['login'],
        'created_at': repo_data['created_at'],
        'repository_name': repo_data['name']
    }
    return data


def _get_github_repository_commit_details(commit_data):
    data = {
        'sha': commit_data['sha'],
        'commit_message': commit_data['commit']['message'],
        'commit_author_name': commit_data['commit']['author']['name']
    }
    return data


def _format_github_repository_commits_url(commits_url):
    """
    It receives commits url from github API and try to replace {/sha}
    within url instead of composing one manually
    """
    return commits_url.replace('{/sha}', '')


def search_github_repositories(search_term):
    """
    It works out on fetching repositories and their last commit details by given `search_term`.
    1. It sends a request to Github API to search repositories by `search_term` argument.
    2. If result is found it sorts repository objects by `created_at` value in descending order
    3. It loops through repositories. It gets `commits_url` and fetch last commit details.

    Error handling:
    1. If fetching repositories fails due to some reason we raise `GithubAPIException`
    2. If fetching repository commit details fails due to some reason (e.g no commits)
    we update `commit_error_message`.

    :param search_term: term to search git repositories through Github API
    :return: list of repositories and their last commit details
    """
    session = GithubProxySession()
    response = session.get('/search/repositories', params='q={}+in:name'.format(search_term))
    data = response.json()

    if response.status_code != HTTP_200_OK:
        raise GithubAPIException(message=data['message'])

    newest_repositories = sorted(data['items'], key=lambda repo: repo['created_at'], reverse=True)[:5]

    result = []
    for repo_data in newest_repositories:
        data = _get_github_repository_details(repo_data)
        commits_url = _format_github_repository_commits_url(repo_data['commits_url'])
        commit_response = session.get(commits_url, params={'per_page': 1})
        commits = commit_response.json()

        if commit_response.status_code != HTTP_200_OK:
            data.update({'commit_error_message': commits['message']})
        else:
            commit_data = commits[0]
            commit_data = _get_github_repository_commit_details(commit_data)
            data.update(commit_data)

        result.append(data)

    return result
